package main

import (
	"testing"
)

func TestPairSum10(t *testing.T) {
	r := findPairSum10([]int{4, 6}, 10)
	if len(r) != 1 {
		t.Errorf("There must be only 1 result, found %d", len(r))
		return
	}

	if r[0][0] != 0 {
		t.Errorf("First index must be 0, found %d", r[0][0])
	}
	if r[0][1] != 1 {
		t.Errorf("First index must be 1, found %d", r[0][1])
	}
}

func TestPairSum10In4(t *testing.T) {
	r := findPairSum10([]int{2, 4, 6, 1}, 10)
	if len(r) != 1 {
		t.Errorf("There must be only 1 result, found %d", len(r))
		return
	}

	if r[0][0] != 1 {
		t.Errorf("First index must be 1, found %d", r[0][0])
	}
	if r[0][1] != 2 {
		t.Errorf("First index must be 2, found %d", r[0][1])
	}
}

func TestPairSum10ThreeResults(t *testing.T) {
	r := findPairSum10([]int{0, 1, 2, 3, 4, 5, 6}, 5)
	if len(r) != 3 {
		t.Errorf("There must be 3 result, found %d", len(r))
		return
	}

	var s1, s2, s3 bool
	for i := 0; i < 3; i++ {
		if r[i][0] == 0 && r[i][1] == 5 {
			s1 = true
		}
		if r[i][0] == 1 && r[i][1] == 4 {
			s2 = true
		}
		if r[i][0] == 2 && r[i][1] == 3 {
			s3 = true
		}
	}

	if !s1 {
		t.Errorf("Solution [0, 5] not found")
	}
	if !s2 {
		t.Errorf("Solution [1, 4] not found")
	}
	if !s3 {
		t.Errorf("Solution [2, 3] not found")
	}
}
