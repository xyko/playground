package main

func main() {
}

func findPairSum10(A []int, s int) [][]int {
	var r [][]int

	for i := 0; i < len(A); i++ {
		for j := i + 1; j < len(A); j++ {
			if A[i]+A[j] == s {
				r = append(r, []int{i, j})
			}
		}
	}

	return r
}
