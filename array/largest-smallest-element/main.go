package main

import "fmt"

func main() {
	fmt.Println("vim-go")
}

func findSmallerLargest(A []int) (int, int) {
	s := A[0]
	l := s

	for _, v := range A {
		if v > l {
			l = v
		}

		if v < s {
			s = v
		}
	}

	return s, l
}
