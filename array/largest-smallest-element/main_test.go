package main

import "testing"

func Test123(t *testing.T) {
	a, b := findSmallerLargest([]int{1, 2, 3})
	if a != 1 {
		t.Errorf("Smallest is 1, found %d", a)
	}
	if b != 3 {
		t.Errorf("Largest is 3, found %d", a)
	}
}

func Test24(t *testing.T) {
	a, b := findSmallerLargest([]int{2, 4})
	if a != 2 {
		t.Errorf("Smallest is 2, found %d", a)
	}
	if b != 4 {
		t.Errorf("Largest is 4, found %d", a)
	}
}

func Test6(t *testing.T) {
	a, b := findSmallerLargest([]int{6})
	if a != 6 {
		t.Errorf("Smallest is 6, found %d", a)
	}
	if b != 6 {
		t.Errorf("Largest is 6, found %d", a)
	}
}
