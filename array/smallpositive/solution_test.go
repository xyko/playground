package solution

import "testing"

func Test1(t *testing.T) {
	x := []int{1, 2, 3}
	r := Solution(x)
	if r != 4 {
		t.Errorf("Result is %d, must be 4", r)
	}
}

func Test2(t *testing.T) {
	x := []int{-100, 100}
	r := Solution(x)
	if r != 1 {
		t.Errorf("Result is %d, must be 1", r)
	}
}

func Test3(t *testing.T) {
	x := []int{-2, -1}
	r := Solution(x)
	if r != 1 {
		t.Errorf("Result is %d, must be 1", r)
	}
}

func Test4(t *testing.T) {
	x := []int{-2, -1, 0}
	r := Solution(x)
	if r != 1 {
		t.Errorf("Result is %d, must be 1", r)
	}
}

func Test5(t *testing.T) {
	x := []int{-2, -1, 0, 1}
	r := Solution(x)
	if r != 2 {
		t.Errorf("Result is %d, must be 1", r)
	}
}

func Test6(t *testing.T) {
	x := []int{}
	r := Solution(x)
	if r != 1 {
		t.Errorf("Result is %d, must be 1", r)
	}
}
func Test7(t *testing.T) {
	x := []int{-10}
	r := Solution(x)
	if r != 1 {
		t.Errorf("Result is %d, must be 1", r)
	}
}
func Test8(t *testing.T) {
	x := []int{10}
	r := Solution(x)
	if r != 1 {
		t.Errorf("Result is %d, must be 1", r)
	}
}
func Test9(t *testing.T) {
	x := []int{1}
	r := Solution(x)
	if r != 2 {
		t.Errorf("Result is %d, must be 1", r)
	}
}
