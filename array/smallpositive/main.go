package solution

// you can also use imports, for example:
// import "fmt"
// import "os"

// you can write to stdout for debugging purposes, e.g.
// fmt.Println("this is a debug message")

import (
	"sort"
)

func Solution(A []int) int {
	if len(A) == 0 {
		return 1
	}

	sort.Ints(A)

	last := -1
	for i, v := range A {
		if i > 0 {
			last = A[i-1]
		}

		if v < 2 {
			continue
		}

		if v-last <= 1 {
			continue
		}

		return max(1, last+1)
	}

	return max(A[len(A)-1]+1, 1)
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
