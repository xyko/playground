package main

import "testing"

func TestSame(t *testing.T) {
	if !areAnagrams("abc", "abc") {
		t.Error("abc is anagram of itself")
	}
}

func TestReversed(t *testing.T) {
	if !areAnagrams("abc", "cba") {
		t.Error("abc and cba are anagrams of one another")
	}
}

func TestNotAnagrams(t *testing.T) {
	if areAnagrams("abc", "Mond") {
		t.Error("abc and Mond are not anagrams of one another")
	}
}
