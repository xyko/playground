package main

import "sort"

func main() {
}

func areAnagrams(a, b string) bool {
	if len(a) != len(b) {
		return false
	}

	sa := make([]int, len(a), len(a))
	sb := make([]int, len(b), len(b))

	for i, c := range a {
		sa[i] = int(c)
	}

	for i, c := range b {
		sb[i] = int(c)
	}

	sort.Ints(sa)
	sort.Ints(sb)

	for i, _ := range sa {
		if sa[i] != sb[i] {
			return false
		}
	}

	return true
}
