package main

import (
	"fmt"
	"log"
	"math"
)

func main() {
	fmt.Println("oi")
}

type connection struct {
	weight int
	child  *node
}

type node struct {
	connections []connection
	visited     bool
	distance    int
	prev        *node
	name        string
}

type graph struct {
	start *node
	nodes []*node
}

func dijkstra(G *graph, dst *node) int {
	log.Printf("src=%s dst=%s", G.start.name, dst.name)

	// initialize graph
	for _, n := range G.nodes {
		n.visited = false
		n.distance = math.MaxInt32
	}

	G.start.distance = 0

	// create list of vertexes
	vertexes := make([]*node, len(G.nodes), len(G.nodes))
	for i, v := range G.nodes {
		vertexes[i] = v
	}

	for {
		if len(vertexes) == 0 {
			return -1
		}

		// find node with smallest distance
		sel_ix, sel := min_node(vertexes)

		// if final node is the selected we found the response
		if sel == dst {
			fmt.Print("result (reversed): ")
			for sel != nil {
				fmt.Printf("%s ", sel.name)
				sel = sel.prev
			}
			fmt.Println("")
			return dst.distance
		}

		// remove from the list of vertexes still not inspected
		vertexes[sel_ix] = vertexes[len(vertexes)-1]
		vertexes = vertexes[:len(vertexes)-1]

		var next *node
		for _, n := range sel.connections {
			if n.child.visited {
				continue
			}

			new_d := sel.distance + n.weight
			cur_d := n.child.distance

			if new_d < cur_d {
				n.child.distance = new_d
				n.child.prev = sel
			}

			next = n.child
		}

		sel.visited = true
		sel = next
	}
}

func min_node(N []*node) (ix int, n *node) {
	sel_ix := 0
	sel := N[sel_ix]
	min_d := sel.distance

	for i, v := range N {
		if v.distance < min_d {
			min_d = v.distance
			sel_ix = i
			sel = v
		}
	}

	return sel_ix, sel
}
