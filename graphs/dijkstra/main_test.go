package main

import (
	"math"
	"testing"
)

func createGraph() *graph {
	n1 := node{name: "1", distance: math.MaxInt32}
	n2 := node{name: "2", distance: math.MaxInt32}
	n3 := node{name: "3", distance: math.MaxInt32}
	n4 := node{name: "4", distance: math.MaxInt32}
	n5 := node{name: "5", distance: math.MaxInt32}
	n6 := node{name: "6", distance: math.MaxInt32}

	n1.connections = []connection{
		connection{child: &n2, weight: 7},
		connection{child: &n3, weight: 9},
		connection{child: &n6, weight: 14},
	}

	n2.connections = []connection{
		connection{child: &n1, weight: 7},
		connection{child: &n3, weight: 10},
		connection{child: &n4, weight: 15},
	}

	n3.connections = []connection{
		connection{child: &n1, weight: 9},
		connection{child: &n2, weight: 10},
		connection{child: &n4, weight: 11},
		connection{child: &n6, weight: 2},
	}

	n4.connections = []connection{
		connection{child: &n2, weight: 15},
		connection{child: &n3, weight: 11},
		connection{child: &n5, weight: 6},
	}

	n5.connections = []connection{
		connection{child: &n4, weight: 6},
		connection{child: &n6, weight: 9},
	}

	n6.connections = []connection{
		connection{child: &n1, weight: 14},
		connection{child: &n3, weight: 9},
		connection{child: &n5, weight: 9},
	}

	return &graph{
		nodes: []*node{&n1, &n2, &n3, &n4, &n5, &n6},
		start: &n1,
	}
}

func TestNulDistance(t *testing.T) {
	G := createGraph()
	end := G.start

	d := dijkstra(G, end)

	if d != 0 {
		t.Errorf("When start and end distance=0, found %d", d)
	}
}

func TestWikipediaExample(t *testing.T) {
	G := createGraph()
	end := G.nodes[4]

	d := dijkstra(G, end)

	if d != 20 {
		t.Errorf("Distance must be 20, found %d", d)
	}
}

func TestModifiedWikipediaExample(t *testing.T) {
	G := createGraph()
	end := G.nodes[5]

	d := dijkstra(G, end)

	if d != 11 {
		t.Errorf("Distance must be 11, found %d", d)
	}
}
