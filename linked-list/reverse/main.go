package main

func main() {
}

type item struct {
	value int
	next  *item
}

func reverse(L *item) *item {
	cur := L
	var last *item

	for cur.next != nil {
		t := cur.next
		cur.next = last
		last = cur
		cur = t
	}

	cur.next = last

	return cur
}
