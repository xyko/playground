package main

import "testing"

func TestRevertSingle(t *testing.T) {
	list := &item{
		value: 1,
		next:  nil,
	}

	r := reverse(list)

	if r != list {
		t.Error("The only element on a one-item list must be the first item on the reverted list")
		return
	}

	if r.next != nil {
		t.Error("The list is not terminated")
	}
}

func TestRevert4(t *testing.T) {
	list := &item{
		value: 1,
		next: &item{
			value: 2,
			next: &item{
				value: 3,
				next: &item{
					value: 4,
					next:  nil,
				},
			},
		},
	}

	r := reverse(list)

	cur := r
	for i := 0; i < 4; i++ {
		if cur == nil {
			t.Errorf("List prematurelly terminated at item %d", i)
			return
		}

		if cur.value != 4-i {
			t.Errorf("Item 1 on the reversed list: expected=%d, found=%d", 2-i, cur.value)
		}
		cur = cur.next
	}

	if cur != nil {
		t.Errorf("List is not terminated")
	}
}
