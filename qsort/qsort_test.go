package main

import (
	"math"
	"math/rand"
	"testing"
)

const large = 10

func Test4321(t *testing.T) {
	var data = []int{4, 3, 2, 1}
	qsort(data, 0, len(data)-1)

	for i, v := range data {
		if i+1 != v {
			t.Errorf("data[%d]=%d (must be %d)", i, v, i+1)
		}
	}
}

func TestLargeSort(t *testing.T) {
	s := large
	d := make([]int, s, s)
	for i, _ := range d {
		d[i] = rand.Int()
	}

	qsort(d, 0, len(d)-1)

	last := math.MinInt32
	for i, v := range d {
		if v < last {
			t.Errorf("d[%d]=%d is bigger than d[%d]=%d", i-1, d[i-1], i, d[i])
		}
	}
}

func TestReverse(t *testing.T) {
	s := large
	d := make([]int, s, s)
	for i, _ := range d {
		d[i] = s - i - 1
	}

	qsort(d, 0, len(d)-1)

	for i, v := range d {
		if d[i] != i {
			t.Errorf("d[%d] must be %d", i, v)
		}
	}
}
