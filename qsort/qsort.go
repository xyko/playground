package main

import "sync/atomic"

func main() {
}

type sort_task struct {
	d  []int
	lo int
	hi int
}

//func qsort(d []int, lo int, hi int) {
//	if hi-lo <= 0 {
//		return
//	}
//
//	p := part(d, lo, hi)
//	qsort(d, lo, p)
//	qsort(d, p+1, hi)
//}

const SortRoutines = 8

func qsort(d []int, lo int, hi int) {
	ch := make(chan sort_task, SortRoutines)
	done := make(chan bool)
	var pending int32

	for i := 0; i < SortRoutines; i++ {
		go sort_r(ch, done, &pending)
	}

	atomic.AddInt32(&pending, 1)
	add_task(ch, d, lo, hi)
}

func add_task(ch chan<- sort_task, d []int, lo, hi int) {
	ch <- sort_task{
		d:  d,
		lo: lo,
		hi: hi,
	}
}

func sort_r(ch <-chan sort_task, done <-chan bool, pending *int32) {
	select {
	case r := <-ch:
		defer atomic.AddInt32(pending, -1)
		if r.hi-r.lo <= 0 {
			return
		}

		p := part(r.d, r.lo, r.hi)

		atomic.AddInt32(pending, 1)
		qsort(r.d, r.lo, p)
		qsort(r.d, p+1, r.hi)
	case <-done:
		return
	}
}

func part(d []int, lo int, hi int) int {
	mid := (hi + lo) / 2
	d[lo], d[mid] = d[mid], d[lo]

	i := lo - 1
	j := hi + 1
	piv := d[lo]

	for {
		for {
			i = i + 1
			if d[i] >= piv {
				break
			}
		}

		for {
			j = j - 1
			if d[j] <= piv {
				break
			}
		}

		if i >= j {
			return j
		}

		d[j], d[i] = d[i], d[j]
	}
}
