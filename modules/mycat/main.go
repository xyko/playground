package main

import (
	"io"
	"os"

	"github.com/sirupsen/logrus"
)

func main() {
	if _, err := io.Copy(os.Stdout, os.Stdin); err != nil {
		logrus.Fatal(err)
	}
}
